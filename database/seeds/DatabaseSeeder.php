<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriasTableSeeder::class);
    	// $this ->truncateTables([
    	// 	'inventario'

    	// ]);
    	
        // $this->call(UsersTableSeeder::class);

        // $this->call(inventario::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);   
         }



//funcion para truncar tablas
    protected function truncateTables(array $tables)
    {

    	  //Desactivamos las llaves foraneas que puedan existir para no tener problemas en truncar tabla
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	//truncamos la tabla para que no exista datos duplicados al volver a querer ingresar los datos
    	foreach ($tables as $table) {

    		DB::table($table)->truncate();
    	}
    	
		DB::statement('SET FOREIGN_KEY_CHECKS=1;'); //activamos nuevamente las llaves foraneas
    	
    }
}
