<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        User::create([
            'name'=> 'Admin',
            'password'=>bcrypt('a1234567'),
            'email'=>'admin@gmail.co',
        ]);


        Role::create([
        'name'    => 'Administrador',
        'slug'    => 'admin.sistema',
        'special' => 'all-access',
        ]);

        Role::create([
        'name'    => 'Vendedor',
        'slug'    => 'vendedor.sistema',
        ]);

        Role::create([
        'name'    => 'Gerente Sucursal',
        'slug'    => 'gerente.sistema',
        ]);

        Role::create([
        'name'    => 'Encargado Bodega',
        'slug'    => 'bodega.sistema',
        ]);

        $user = User::find(1); //Italo Morales
        $user->roles()->sync(1);
    }
}
