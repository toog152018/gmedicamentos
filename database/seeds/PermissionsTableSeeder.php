<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permisos sobre los usuarios
        Permission::create([
        'name'        => 'Navegar usuarios',
        'slug'        => 'users.index',
        'description' => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
        'name'		  => 'Ver detalles de usuarios',
        'slug' 		  => 'users.show',
        'description' => 'Ver en detalle cada usuario del sistema',
        ]);

        Permission::create([
        'name' 		  => 'Edición de  usuarios',
        'slug' 		  => 'users.edit',
        'description' => 'Editar cualquier dato de un usuario del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar usuarios',
        'slug'        => 'users.destroy',
        'description' => 'Eliminar cualquier usuario del sistema',
        ]);

        //Permisos sobre los Roles
        Permission::create([
        'name'        => 'Navegar roles',
        'slug'        => 'roles.index',
        'description' => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de roles',
        'slug'        => 'roles.show',
        'description' => 'Ver en detalle cada rol del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  roles',
        'slug'        => 'roles.create',
        'description' => 'Crear cualquier rol del sistema',
        ]);

        Permission::create([
        'name'        => 'Edición de  roles',
        'slug'        => 'roles.edit',
        'description' => 'Editar cualquier dato de un rol del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar roles',
        'slug'        => 'roles.destroy',
        'description' => 'Eliminar cualquier rol del sistema',
        ]);

        /**Permisos sobre las categorías
        Permission::create([
        'name'        => 'Navegar categorias',
        'slug'        => 'categorias.index',
        'description' => 'Lista y navega todos las categorias del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de categorias',
        'slug'        => 'categorias.show',
        'description' => 'Ver en detalle cada categoría del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  categorias',
        'slug'        => 'categorias.create',
        'description' => 'Crear cualquier categoría del sistema',
        ]);

        Permission::create([
        'name'        => 'Edición de  categorias',
        'slug'        => 'categorias.edit',
        'description' => 'Editar cualquier dato de una categoría del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar categorias',
        'slug'        => 'categorias.destroy',
        'description' => 'Eliminar cualquier categoría del sistema',
        ]);

        //Permisos sobre las presentaciones
        Permission::create([
        'name'        => 'Navegar presentaciones',
        'slug'        => 'presentaciones.index',
        'description' => 'Lista y navega todos las presentaciones del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de presentaciones',
        'slug'        => 'presentaciones.show',
        'description' => 'Ver en detalle cada presentación del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  presentaciones',
        'slug'        => 'presentaciones.create',
        'description' => 'Crear cualquier presentación del sistema',
        ]);

        Permission::create([
        'name'        => 'Edición de  presentaciones',
        'slug'        => 'presentaciones.edit',
        'description' => 'Editar cualquier dato de una presentación del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar presentaciones',
        'slug'        => 'presentaciones.destroy',
        'description' => 'Eliminar cualquier presentación del sistema',
        ]);        

        //Permisos sobre las compras
        Permission::create([
        'name'        => 'Navegar compras',
        'slug'        => 'compras.index',
        'description' => 'Lista y navega todos las compras del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de compras',
        'slug'        => 'compras.show',
        'description' => 'Ver en detalle cada compra del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  compras',
        'slug'        => 'compras.create',
        'description' => 'Crear cualquier compra del sistema',
        ]);

        //Permisos sobre los lotes
        Permission::create([
        'name'        => 'Navegar lotes',
        'slug'        => 'lotes.index',
        'description' => 'Lista y navega todos las lotes del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de lotes',
        'slug'        => 'lotes.show',
        'description' => 'Ver en detalle cada lote del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  lotes',
        'slug'        => 'lotes.create',
        'description' => 'Crear cualquier lote del sistema',
        ]);

        Permission::create([
        'name'        => 'Edición de  lotes',
        'slug'        => 'lotes.edit',
        'description' => 'Editar cualquier dato de un lote del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar lotes',
        'slug'        => 'lotes.destroy',
        'description' => 'Eliminar cualquier lote del sistema',
        ]);

        //Permisos sobre las inventario
        Permission::create([
        'name'        => 'Navegar inventario',
        'slug'        => 'inventario.index',
        'description' => 'Lista y navega los inventarios del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de inventario',
        'slug'        => 'inventario.show',
        'description' => 'Ver en detalle cada inventario del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  inventario',
        'slug'        => 'inventario.create',
        'description' => 'Crear cualquier inventario del sistema',
        ]);

        //Permisos sobre las sucursales
        Permission::create([
        'name'        => 'Navegar sucursales',
        'slug'        => 'sucursales.index',
        'description' => 'Lista y navega todos las sucursales administradas por el sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de sucursales',
        'slug'        => 'sucursales.show',
        'description' => 'Ver en detalle cada sucursal',
        ]);

        Permission::create([
        'name'        => 'Creación de  sucursales',
        'slug'        => 'sucursales.create',
        'description' => 'Crear cualquier sucursal',
        ]);

        Permission::create([
        'name'        => 'Edición de  sucursales',
        'slug'        => 'sucursales.edit',
        'description' => 'Editar cualquier dato de una sucursal',
        ]);

        Permission::create([
        'name'        => 'Eliminar sucursales',
        'slug'        => 'sucursales.destroy',
        'description' => 'Eliminar cualquier sucursal administrada por el sistema',
        ]);*/

        /**Productos
        Permission::create([
        'name'        => 'Navegar productos',
        'slug'        => 'products.index',
        'description' => 'Lista y navega todos los productos del sistema',
        ]);

        Permission::create([
        'name'        => 'Ver detalles de productos',
        'slug'        => 'products.show',
        'description' => 'Ver en detalle cada producto del sistema',
        ]);

        Permission::create([
        'name'        => 'Creación de  productos',
        'slug'        => 'products.create',
        'description' => 'Crear cualquier producto del sistema',
        ]);

        Permission::create([
        'name'        => 'Edición de  productos',
        'slug'        => 'products.edit',
        'description' => 'Editar cualquier dato de un producto del sistema',
        ]);

        Permission::create([
        'name'        => 'Eliminar productos',
        'slug'        => 'products.destroy',
        'description' => 'Eliminar cualquier producto del sistema',
        ]);*/

        

    }
}
