<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('registro', function (Blueprint $table) {
            $table->increments('id');

            //Llave a la tabla catalogo existencias
            $table->integer('idMedicamento')->unsigned();
            $table->foreign('idMedicamento')->references('id')->on('catalogo_existencia')->onDelete('cascade');


            $table->decimal('precio');
            $table->integer('cantidad');
            $table->decimal('total');
            $table->string('tipo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('registro');

    }
}
