<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoExistencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('catalogo_existencia', function (Blueprint $table) {
            $table->increments('id');

            //Llave a la tabla medicamentos
            $table->integer('idMedicamento')->unsigned();
            $table->foreign('idMedicamento')->references('id')->on('medicamentos');

            //Llave a la tabla lotes
            $table->integer('idLote')->unsigned();
            $table->foreign('idLote')->references('id')->on('lotes');

            $table->decimal('precioC');
            $table->decimal('precioV');
            $table->integer('cantidad');
            $table->decimal('descuento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('catalogo_existencia');

    }
}
