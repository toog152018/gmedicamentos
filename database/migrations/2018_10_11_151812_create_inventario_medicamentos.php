<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioMedicamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //se crea la tabla para el inventario de medicamentos
            Schema::create ('inventario', function (Blueprint $table) {
            $table->increments('idmedicamento')->index();
             $table->integer('idsucursal'); 
            $table->integer('cantidadMin');
            $table-> string('Nombre');
            $table->timestamp('fecha_registro');
            
         }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('inventario');
    }
}
