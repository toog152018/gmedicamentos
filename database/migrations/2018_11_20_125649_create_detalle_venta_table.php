<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_ventas', function (Blueprint $table) {
            $table->increments('id');

            //Llave a la tabla ventas
            $table->integer('idVenta')->unsigned();
            $table->foreign('idVenta')->references('id')->on('ventas');

            //Llave a la tabla catalogo existencias
            $table->integer('idProducto')->unsigned();
            $table->foreign('idProducto')->references('id')->on('catalogo_existencia');

            $table->decimal('precioUni');
            $table->integer('cantidad');
            $table->decimal('precio');
            $table->decimal('descuento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_venta');
    }
}
