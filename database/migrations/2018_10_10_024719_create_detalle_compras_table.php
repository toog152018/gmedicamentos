<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleCompras', function (Blueprint $table) {

            $table->increments('id');
            
            //llave foranea a compra
            $table->integer('compras_id')->unsigned();
            $table->foreign('compras_id')->references('id')->on('compras');

            //llave foranea a Lote
            $table->integer('lotes_id')->unsigned();
            $table->foreign('lotes_id')->references('id')->on('lotes');

            $table->decimal('precio');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalleCompras');
    }
}
