<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //se crea la tabla para los medicamentos
            Schema::create ('medicamentos', function (Blueprint $table) {
            $table->increments('id')->index();

            $table->integer('idSucursal')->unsigned();
            $table->foreign('idSucursal')->references('id')->on('sucurcales');

            $table->integer('idPresentacion')->unsigned();
            $table->foreign('idPresentacion')->references('id')->on('presentacions');

            $table->integer('idCategoria')->unsigned();
            $table->foreign('idCategoria')->references('id')->on('categorias');

            $table-> string('Nombre');
            $table->timestamps();
            
         }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('medicamentos');
    }
}
