@extends('layouts.layout')

@section('title')
Compra - Edicion
@endsection

@section('content')
<h2>Edicion de: {{$compra->id}}</h2>
<form class="form-inline" style="width:100%" action="{{ route('compras.update', $compra->id)}}" method="POST">
	{{ csrf_field() }}

	<input name="_method" value="PUT" type="hidden"> 

	<div class="form-group">
		<label for="fecha">Fecha:</label><br>
		<input type="date" class="form-control" style="width:100%" name="fecha" value="{{ $compra->fecha }}">
	</div><br><br>
	<div class="form-group">
		<label for="proveedor">Proveedor:</label><br>
		<input type="text" class="form-control" style="width:100%" name="proveedor" value="{{ $compra->proveedor }}">
	</div><br><br>
	<div class="form-group">
		<label for="total">Total:</label><br>
		<input type="number" class="form-control" style="width:100%" name="total" value="{{ $compra->total }}">
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<button type="button" class="btn btn-secondary"><a style="text-decoration:none;color:black" href="{{ route('compras.index')}}">Cancelar</a></button>
</form>
@endsection