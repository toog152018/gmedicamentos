@extends('layouts.layout')

@section('title')
Compra - Nuevo
@endsection

@section('content')
<h2>Nueva Compra</h2>
<form class="form-inline" style="width:100%" action="{{ route('compras.store' )}}" method="POST">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="fecha">Fecha :</label><br>
		<input type="date" class="form-control"  name="fecha">
	</div>
	<div class="form-group">
		<label for="proveedor">Proveedor :</label><br>
		<input type="text" class="form-control"  name="proveedor">
	</div><br><br>
	<table class="table bg-info"  id="tabla">
					<tr class="fila-fija">
						<td><input required name="descripcion[]" type="text" placeholder="Descripcion"/></td>
						<td><input required name="fechaVencimiento[]" 
						type="date"	placeholder="Fecha de vencimiento"/></td>
						<td><input required name="precio[]" 
						type="number"	step="0.01" placeholder="Precio"/></td>
						<td><input required name="cantidad[]" 
						type="number"	step="1" placeholder="Cantidad"/></td>
						<td class="eliminar "><button type="button" class="btn btn-danger">Menos - </button></td>
					</tr>
				</table>

	<button type="submit" class="btn btn-success">Guardar</button>
	<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Más + </button>
	<button type="button" class="btn btn-secondary"><a style="text-decoration:none;color:black" href="{{ route('compras.index')}}">Cancelar</a></button>
</form>

<script>
			
    		$(function(){
				// Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
				$("#adicional").on('click', function(){
					$("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
				});
			 
				// Evento que selecciona la fila y la elimina 
				$(document).on("click",".eliminar",function(){
					var parent = $(this).parents().get(0);
					$(parent).remove();
				});
			});
		</script>
@endsection