@extends('layouts.layout')
@section('title')
Compra - Detalle
@endsection

@section('content')
<h2>Detalles Compra</h2>
		
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th>Descripcion</th>
				<th>Fecha de vencimiento</th>
				<th>Precio</th>
				<th>Total</th>
			</tr>
			<tbody>
				@foreach($arreglo as $de)
				<tr>
					<td>{{ $de['descripcion'] }}</td>
					<td>{{ $de['fechaVencimiento'] }}</td>
					<td>{{ $de['precio'] }}</td>
					<td>{{ $de['cantidad'] }} </td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
		<button type="button" class="btn btn btn-success"><a style="text-decoration:none;color:black" href="{{ route('compras.index')}}">Regresar</a></button>


@endsection