@extends('layouts.layout')

@section('title')
Compras
@endsection

@section('title-1')
GESTION DE COMPRAS
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			Lista de compras
			<a href="{{ route('compras.create')}}" class="btn btn-primary pull-right">Nuevo</a>
		</h2>		

		@include('compras.fragment.info')
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Fecha </th>
				<th>proveedor</th>
				<th>total</th>
				<th colspna="3">Acciones</th>
			</tr>
			<tbody>
				@foreach($compras as $compra)
				<tr>
					<td>{{ $compra->id }}</td>
					<td>{{ $compra->fecha }}</td>
					<td>{{ $compra->proveedor }}</td>
					<td>{{ $compra->total}} </td>
					<td>
						<a href="{{ route('compras.show', $compra->id)}}" class="btn btn-warning pull-right">Detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{!! $compras->render() !!}
	</div>
@endsection