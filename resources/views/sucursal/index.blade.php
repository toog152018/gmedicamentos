@extends('layouts.layout')

@section('title')
Sucursal
@endsection

@section('title-1')
GESTION DE SUCURSALES
@endsection

@section('content')
<div class="pull-left"><h3>Gestion de Sucursales</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('sucursal.create') }}" class="btn btn-info" >Nueva Sucursal</a>
            </div>
          </div>	
<div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               	<th width="20px">ID</th>
				<th>Nombre de la farmacia</th>
				<th>Lugar/ubicacion</th>
				<th>Encargado</th>
				
				<th colspna="3">Acciones</th>

             </thead>
             <tbody>
             	@foreach($sucursales as $sucursales)
				<tr>
					<td>{{ $sucursales->id }}</td>
					<td>{{ $sucursales->nombre}}</td>
					<td>{{ $sucursales->lugar}}</td>
					<td>{{ $sucursales->encargado}} </td>
					<td><a class="btn btn-primary btn-xs" href="{{action('SucursalController@edit', $sucursales->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('SucursalController@destroy', $sucursales->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">
 
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
                @endforeach
            </tbody>
 
          </table>
        </div>
     
      
@endsection