@extends('layouts.layout')

@section('title')
Sucursal
@endsection

@section('title-1')
GESTION DE SUCURSALES
@endsection

@section('content')
	
<div class="table-container">
            <form method="POST" action="{{ route('sucursal.update',$sucursal->id) }}"  role="form">
              {{ csrf_field() }}
              <input name="_method" type="hidden" value="PATCH">
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label>Nombre de la sucursal: </label>
                    <input type="text" name="nombre" id="nombre" class="form-control input-sm" value="{{$sucursal->nombre}}">
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label>Lugar/Direccion: </label>
                    <input type="text" name="lugar" id="lugar" class="form-control input-sm" value="{{$sucursal->lugar}}">
                  </div>
                </div>
              </div>

              <div class="row">

                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <label>Nombre del encargado: </label>
                    <input type="text" name="encargado" id="encargado" class="form-control input-sm" value="{{$sucursal->encargado}}">
                  </div>
                </div>
              </div>

              <div class="row">

                <div class="col-xs-1 col-sm-1 col-md-1">
                  <div class="form-group">
                  <input type="submit"  value="Actualizar" class="btn btn-success btn-block">
                  </div>  
                  </div> 
                  <div class="col-xs-1 col-sm-1 col-md-1">
                  <div class="form-group"> 
                  <a href="{{ route('sucursal.index') }}" class="btn btn-info btn-block" >Atrás</a>
                </div>  

              </div>
            </form>
          </div>
     
      
@endsection