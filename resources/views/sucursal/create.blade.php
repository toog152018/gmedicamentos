@extends('layouts.layout')

@section('title')
Nueva sucursal
@endsection

@section('content')
<h2>Ingresar sucursal</h2>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" action="{{ route('sucursal.store') }}"  role="form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombre de la sucursal: </label>
										<input type="text" name="nombre" id="nombre" class="form-control input-sm" placeholder="Nombre de la sucursal">
									</div>
								</div>
								
							</div>
 							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										
										<label>Lugar/Direccion: </label>
										<input type="text" name="lugar" id="lugar" class="form-control input-sm" placeholder="lugar/Direccion">
								
							
									</div>
								</div>
						
							</div>
 						
							
							<div class="form-group">
								<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Nombre del encargado: </label>
										<input type="text" name="encargado" id="encargado" class="form-control input-sm" placeholder="Nombre del Encargado">
									</div>
								</div>
						
							</div>
						
							<div class="row">
 
								 <div class="col-xs-1 col-sm-1 col-md-1">
                 					 <div class="form-group">
									<input type="submit"  value="Guardar" class="btn btn-success btn-block" width="20px">
								</div></div>	
								 <div class="col-xs-1 col-sm-1 col-md-1">
                 					 <div class="form-group">
									<a href="{{ route('sucursal.index') }}" class="btn btn-info btn-block" width="20px">Atrás</a>
								</div>	
								</div>
 
							</div>
						</form>
					</div>
@endsection