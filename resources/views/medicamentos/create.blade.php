@extends('layouts.layout')

@section('title')
Medicamento - Nuevo
@endsection

@section('content')
<h2>Nuevo medicamento</h2>
<form class="form-inline" style="width:100%" action="{{ route('medicamentos.store' )}}" method="POST">
	{{ csrf_field() }}

	<div class="form-group">
		<label for="nombre">Nombre:</label><br>
		<input type="text" class="form-control" style="width:100%" name="nombre" required>
	</div><br><br>
	<div class="form-group" >
		<label for="nombre">Presentacion:</label><br>
		<select class="form-control" name="presentacion" required>
			<option value=""  disabled >Por favor seleccione una opcion</option>
		@foreach($Presentaciones as $presentacion)
				<option value="{{$presentacion->id}}">{{$presentacion->nombre}}</option>
			@endforeach
		</select>
	</div><br><br>


	<div class="form-group" >
		<label for="nombre">Categoria:</label><br>
		<select class="form-control" name="categoria" required>
			<option value="" disabled>Por favor seleccione una opcion</option>
			@foreach($Categorias as $category)
				<option value="{{$category->id}}">{{$category->nombre}}</option>
			@endforeach
		</select>
	</div><br><br>



	<button type="submit" class="btn btn-success">Guardar</button>
	<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('medicamentos.index')}}">Cancelar</a>
</form>
@endsection