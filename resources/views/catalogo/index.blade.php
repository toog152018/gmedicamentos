@extends('layouts.layout')

@section('title')
	Inventario
@endsection

@section('title-1')
	Catalogo de existencias
@endsection

@section('content')

	<div class="clo-sm-8">
		<h2>
			<div style="display:inline;opacity:0">.</div>
			<div style="display:inline"><a href="{{ route('catalogo.create')}}" class="btn btn-primary pull-right">Nuevo ingreso</a></div>

		</h2>

		<form class="navbar-form navbar-left" role="search" action="{{url('catalogo')}}">
			<div class="form-group">
				<input type="text" class="form-control" name='nombre' placeholder="Buscar ..." />
			</div>
			<button type="submit" class="btn btn-default">Buscar</button>
		</form>
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Medicamento</th>
				<th>Lote</th>
				<th>Precio Compra</th>
				<th>Precio Venta</th>
				<th>Cantdad</th>
				<th>Descuento</th>
				{{--<th colspna="3">Acciones</th>--}}
			</tr>
			<tbody>
			@foreach($catalogo as $cata)
				<tr>
					<td>{{ $cata->id }}</td>
					<td >{{ $cata->Nombre }}</td>
					<td >{{ $cata->lotes->descripcion}}</td>
					<td>{{$cata->precioC}}</td>
					<td>{{$cata->precioV}}</td>
					<td>{{$cata->cantidad}}</td>
					<td>{{$cata->descuento*100}}%</td>
					{{--                    <td>
                                            <a href="{{ route('catalogo.edit', $cata->id)}}" class="btn btn-warning pull-right">Editar</a>
                                        </td>--}}
				</tr>
			@endforeach
			</tbody>
			</thead>
		</table>
		{{--{{$catalogo->render() }}--}}
	</div>
@endsection
