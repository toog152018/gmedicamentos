@extends('layouts.layout')

@section('title')
	Inventario
@endsection

@section('title-1')
	Entrada de catalogo de existencias
@endsection

@section('content')

	<h2>Nueva Entrada</h2>

	<form class="form-inline" style="width:100%" action="{{ route('catalogo.store' )}}" method="POST">
		{{ csrf_field() }}


		<div class="form-group" >
			<label for="nombre">Medicamento:</label><br>
			<select class="form-control" name="medicamento" required>
				<option value="" disabled>Por favor seleccione una opcion</option>
				@foreach($medicamentos as $medi)
					<option value="{{$medi->id}}">{{$medi->Nombre}}</option>
				@endforeach
			</select>
		</div><br><br>

		<div class="form-group" >
			<label for="nombre">Lote:</label><br>
			<select class="form-control" name="lote" required>
				<option value="" disabled>Por favor seleccione una opcion</option>
				@foreach($lotes as $lote)
					<option value="{{$lote->id}}">{{$lote->descripcion}}</option>
				@endforeach
			</select>
		</div><br><br>

		<div class="form-group">
			<label for="nombre">Precio de compra:</label><br>
			<input type="number" pattern="\d+(\.\d{2})" class="form-control" name="precioC" onkeypress="return numeros(event)" step=".01">
		</div><br><br>
		<div class="form-group">
			<label for="nombre">Cantidad:</label><br>
			<input type="number" class="form-control"  name="cantidad" pattern="[0-9]{10}" onkeypress="return numeros(event)">
		</div><br><br>

		<div class="form-group">
			<label for="nombre">Descuento:</label><br>
			<input type="number" class="form-control"  name="descuento" pattern="[0-9]{10}" onkeypress="return numeros(event)" min="0" step="0.01" max="0.30">
		</div><br><br>

		<button type="submit" class="btn btn-success">Guardar</button>
		<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('catalogo.index')}}">Cancelar</a>
	</form>


	<script>
        function numeros(e){
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = " 0123456789";
            especiales = [8,37,39,46];

            tecla_especial = false
            for(var i in especiales){
                if(key == especiales[i]){
                    tecla_especial = true;
                    break;
                }
            }

            if(letras.indexOf(tecla)==-1 && !tecla_especial)
                return false;
        }


	</script>
@endsection