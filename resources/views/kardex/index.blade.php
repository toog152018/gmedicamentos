@extends('layouts.layout')

@section('title')
	Kardex
@endsection

@section('title-1')
	Kardex
@endsection

@section('content')

	<div class="clo-sm-8">
		{{--<h2>
			<div style="display:inline;opacity:0">.</div>
			<div style="display:inline"><a href="{{ route('kardex.create')}}" class="btn btn-primary pull-right">Nuevo ingreso</a></div>

		</h2>		--}}

		<table class="table table-hover table-striped">
			<tr>
				<th>             </th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th>Entrada</th>
				<th></th>
				<th>Salidad</th>
			</tr>
		</table>
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Medicamento</th>
				<th>Precio</th>
				<th>Cantidad</th>
				<th>Total</th>
				<th>Precio</th>
				<th>Cantidad</th>
				<th>Total</th>
				{{--<th colspna="3">Acciones</th>--}}
			</tr>
			<tbody>
			@foreach($registros as $reg)
				<tr>
					<td>{{ $reg->id }}</td>
					<td>{{ $reg->catalogoExistencias->medicamentos->Nombre }}</td>
					@if($reg->tipo=='IN')
					<td>{{ $reg->precio}}</td>
					<td>{{$reg->cantidad}}</td>
					<td>{{$reg->total}}</td>
						<td></td>
						<td></td>
						<td></td>
						@else
					<td></td>
					<td></td>
					<td></td>
					<td>{{ $reg->precio}}</td>
					<td>{{$reg->cantidad}}</td>
					<td>{{$reg->total}}</td>
						@endif
				</tr>
			@endforeach
			</tbody>
			</thead>
		</table>
		{!! $registros->render() !!}
	</div>
@endsection