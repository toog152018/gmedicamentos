@extends('layouts.layout')

@section('title')
Categorias - Edicion
@endsection

@section('content')
<h2>Edicion de: {{$categoria->nombre}}</h2>
<form class="form-inline" style="width:100%" action="{{ route('categorias.update', $categoria->id)}}" method="POST">
	{{ csrf_field() }}

	<input name="_method" value="PUT" type="hidden"> 

	<div class="form-group">
		<label for="nombre">Nombre:</label><br>
		<input type="text" class="form-control" style="width:100%" name="nombre" value="{{ $categoria->nombre }}">
	</div><br><br>
	<div class="form-group">
		<label for="nombre">Descripcion:</label><br>
		<textarea class="form-control" style="width:110%" name="descripcion" value="{{ $categoria->descripcion }}" rows="6">{{ $categoria->descripcion }}</textarea>
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('categorias.index')}}">Cancelar</a>
</form>
@endsection