@extends('layouts.layout')

@section('title')
Categorias
@endsection

@section('title-1')
Lista de categorias
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			<div style="display:inline;opacity:0">.</div>
			<div style="display:inline"><a href="{{ route('categorias.create')}}" class="btn btn-primary pull-right">Nuevo</a></div>
			
		</h2>		

		@include('categorias.fragment.info')
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th colspna="3">Acciones</th>
			</tr>
			<tbody>
				@foreach($categorias as $categoria)
				<tr>
					<td>{{ $categoria->id }}</td>
					<td style="width:27%">{{ $categoria->nombre }}</td>
					<td style="width:100%">{{ $categoria->descripcion }}</td>
					{{-- <td>
						<a href="{{ route('categorias.show', $categoria->id)}}" class="btn btn-info pull-right">Ver</a>
					</td> --}}
					<td>
						<a href="{{ route('categorias.edit', $categoria->id)}}" class="btn btn-warning pull-right">Editar</a>
					</td>
					<td>
						<form action="{{ route('categorias.destroy', $categoria->id)}}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method"value="DELETE">
						<button class="btn btn-danger">Eliminar</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{!! $categorias->render() !!}
	</div>
@endsection