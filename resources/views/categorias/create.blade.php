@extends('layouts.layout')

@section('title')
Categoria - Nueva
@endsection

@section('content')
<h2>Nueva categoria</h2>
<form class="form-inline" style="width:100%" action="{{ route('categorias.store' )}}" method="POST">
	{{ csrf_field() }}

	<div class="form-group">
		<label for="nombre">Nombre:</label><br>
		<input type="text" class="form-control" style="width:100%" name="nombre" >
	</div><br><br>
	<div class="form-group">
		<label for="nombre">Descripcion:</label><br>
		<textarea class="form-control" style="width:110%" name="descripcion" rows="6"></textarea>
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('categorias.index')}}">Cancelar</a>
</form>
@endsection