@extends('layouts.layout')

@section('title')
Presentaciones
@endsection

@section('title-1')
Lista de presentaciones
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			<div style="display:inline;opacity:0">.</div>
			<div style="display:inline"><a href="{{ route('presentaciones.create')}}" class="btn btn-primary pull-right">Nuevo</a></div>
		</h2>		

		@include('presentaciones.fragment.info')
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th colspna="3">Acciones</th>
			</tr>
			<tbody>
				@foreach($presentaciones as $presentacion)
				<tr>
					<td>{{ $presentacion->id }}</td>
					<td style="width:27%">{{ $presentacion->nombre }}</td>
					<td style="width:100%">{{ $presentacion->descripcion }}</td>
					<td>
						<a href="{{ route('presentaciones.edit', $presentacion->id)}}" class="btn btn-warning pull-right">Editar</a>
					</td>
					<td>
						<form action="{{ route('presentaciones.destroy', $presentacion->id)}}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method"value="DELETE">
						<button class="btn btn-danger">Eliminar</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{!! $presentaciones->render() !!}
	</div>
@endsection