@extends('layouts.layout')

@section('title')
Presentaciones - Nueva
@endsection

@section('content')
<h2>Nueva presentacion</h2>
<form class="form-inline" style="width:100%" action="{{ route('presentaciones.store' )}}" method="POST">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="nombre">Nombre:</label><br>
		<input type="text" class="form-control" required="" tyle="width:100%" name="nombre">
	</div><br><br>
	<div class="form-group">
		<label for="nombre">Descripcion:</label><br>
		<textarea class="form-control" required="" style="width:110%" name="descripcion" rows="6"></textarea>
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('presentaciones.index')}}">Cancelar</a>
</form>
@endsection