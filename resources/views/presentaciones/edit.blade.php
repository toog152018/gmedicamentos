@extends('layouts.layout')

@section('title')
Presentaciones - Edicion
@endsection

@section('content')
<h2>Edicion de: {{$presentacion->nombre}}</h2>
<form class="form-inline" style="width:100%" action="{{ route('presentaciones.update', $presentacion->id)}}" method="POST">
	{{ csrf_field() }}

	<input name="_method" value="PUT" type="hidden"> 

	<div class="form-group">
		<label for="nombre">Nombre:</label><br>
		<input type="text" class="form-control" required="" style="width:100%" name="nombre" value="{{ $presentacion->nombre }}">
	</div><br><br>
	<div class="form-group">
		<label for="nombre">Descripcion:</label><br>
		<textarea class="form-control" required="" style="width:110%" name="descripcion" value="{{ $presentacion->descripcion }}" rows="6">{{ $presentacion->descripcion }}</textarea>
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<a class="btn btn-default" style="text-decoration:none;color:black" href="{{ route('presentaciones.index')}}">Cancelar</a>
</form>
@endsection