@extends('layouts.layout')

@section('title')
vendedores
@endsection

@section('title-1')
Vendedores
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			Lista de vendedores
		</h2>		

	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Nombre del vendedor</th>
				<th colspna="3">Acciones</th>
			</tr>
			<tbody>
				@foreach($vendedor as $ven)
				<tr>
					<td>{{ $ven->id }}</td>
					<td>{{ $ven->name }}</td>

					<td>
						<a href="{{ route('vendedor.show', $ven->id)}}" class="btn btn-warning pull-right">Detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{{$vendedor->render()}}
	</div>
@endsection