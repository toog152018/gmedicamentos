<!-- @extends('layouts.layout')

@section('title') --> 
Inventario - Edicion
<!-- @endsection

@section('content') -->
<h2>Edicion de: <!-- {{$inventario->idmedicamento}}  -->
</h2>
<form class="form-inline" style="width:100%" action="{{ route('inventario.update', $inventario->id)}}" method="POST">
	<!-- {{ csrf_field() }} -->

	<input name="_method" value="PUT" type="hidden"> 

	<div class="form-group">
		<label for="idmedicamento">ID Medicamento:</label><br>
		<input type="text" class="form-control" style="width:100%" name="idmedicamento" value="{{ $inventario->idmedicamento }}">
	</div><br><br>
	<div class="form-group">
		<label for="Nombre">Nombre:</label><br>
		<input type="number" class="form-control" style="width:100%" name="Nombre" value="{{ $inventario->Nombre }}">
	</div><br><br>
	<div class="form-group">
		<label for="cantidad">Cantidad:</label><br>
		<input type="number" class="form-control" style="width:100%" name="cantidad" value="{{ $inventario->cantidad }}">
	</div><br><br>

	<div class="form-group">
		<label for="fecha">Fecha modificacion:</label><br>
		<input type="date" class="form-control" style="width:100%" name="fecha" value="{{ $inventario->fecha }}">
	</div><br><br>

	<button type="submit" class="btn btn-success">Guardar</button>

	<button type="button" class="btn btn-secondary"><a style="text-decoration:none;color:black" href="{{ route('inventarios.index')}}">Cancelar</a></button>
</form>
<!-- @endsection