@extends('layouts.layout')

@section('title')
Inventario - Nuevo
@endsection

@section('content')
<h2>Ingreso de medicamento</h2>
<form class="form-inline" style="width:100%" action="{{ route('inventario.store' )}}" method="POST">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="fecha">Fecha :</label><br>
		<input type="date" class="form-control"  name="fecha">
	</div><br>
	<div class="form-group">
		<label for="proveedor">Nombre :</label><br>
		<input type="text" class="form-control"  name="proveedor">
	</div><br>
	<div class="form-group">
		<label for="proveedor">Cantidad de producto :</label><br>
		<input type="text" class="form-control"  name="proveedor">
	</div><br>

	<button type="submit" class="btn btn-success">Guardar</button>
	<button type="button" class="btn btn-secondary"><a style="text-decoration:none;color:black" href="{{ route('inventario.index')}}">Cancelar</a></button>
</form>

@endsection