@extends('layouts.layout')
@section('title')
INVENTARIO
@endsection

@section('content') -->
<h2>Detalle Inventario</h2>
    
  <table class="table table-hover table-striped">
    <thead>
      <tr>
        <th>ID Medicamento</th>
        <th>ID sucursal</th>
        <th>Cantidad </th>
        <th>Nombre</th>
        <th>Fecha ingreso</th>
      </tr>
      <tbody>
        @foreach($arreglo as $de)
        <tr>
          <td>{{ $de['idmedicamento'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </thead>
  </table>
    <button type="button" class="btn btn btn-success"><a style="text-decoration:none;color:black" href="{{ route('inventario.index')}}">Regresar</a></button>



 @endsection