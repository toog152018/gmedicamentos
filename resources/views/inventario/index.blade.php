@extends('layouts.layout')

@section('title')
Inventario
@endsection

@section('title-1')
GESTION DE INVENTARIO/MEDICAMENTO
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			Lista de inventarios
			<a href="{{ route('inventario.create')}}" class="btn btn-primary pull-right">Nuevo</a>
		</h2>		

		@include('inventario.fragment.info')
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID Medicamento</th>
				<th>ID sucursal</th>
				<th>Cantidad Minima</th>
				<th>Nombre</th>
				<th>Fecha Modificacion</th>
				
				<th colspna="3">Acciones a realizar</th>
			</tr>
			<tbody>
				@foreach($inventario as $inventario)
				<tr>
					<td>{{ $inventario->idmedicamento }}</td>
					<td>{{ $inventario->idsucursal }}</td>
					<td>{{ $inventario->cantidadMin }}</td>
					<td>{{ $inventario->Nombre}} </td>
					<td>{{ $inventario->fecha_registro}} </td>
					<td>
						<a href="{{ route('inventario.show', $inventario->id)}}" class="btn btn-warning pull-right">Ver Detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{!! $inventario->render() !!}
	</div>
@endsection