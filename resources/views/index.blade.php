@extends('layouts.layout')

@section('title') -->
inventarios
<!-- @endsection

@section('title-1') -->
Lista de inventarios
<!-- @endsection

@section('content') -->
	
	<div class="clo-sm-8">
		<h2>
			<a href="{{ route('inventarios.create')}}" class="btn btn-primary pull-right">Nuevo</a>
		</h2>		

		<!-- @include('inventarios.fragment.info') -->
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th colspan="2">Acciones</th>
			</tr>
			<tbody>
			<!-- 	@foreach($inventarios as $inventario) -->
				<tr>
					<td>{{ $inventario->id }}</td>
					<td style="width:27%">{{ $inventario->nombre }}</td>
					<td style="width:100%">{{ $inventario->cantidad }}</td>
					<td>
						<a href="{{ route('inventarios.edit', $inventario->idmedicamento)}}" class="btn btn-warning pull-right">Editar</a>
					</td>
					<td>
						<form action="{{ route('inventarios.destroy', $inventario->idmedicamento)}}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_method"value="DELETE">
						<button class="btn btn-danger">Eliminar</button>
						</form>
					</td>
				</tr>
			<!-- 	@endforeach -->
			</tbody>
		</thead>
	</table>
	<!-- {!! $inventarios->render() !!} -->
	</div>
<!-- @endsection