@extends('layouts.layout')

@section('title')
    Ventas
@endsection

@section('title-1')
    GESTION DE VENTAS
@endsection

@section('content')
<form class="navbar-form navbar-left" role="search" action="{{url('ventas/buscador')}}">
 <div class="form-group">
  <input type="text" class="form-control" name='nombre' placeholder="Buscar ..." />
 </div>
 <button type="submit" class="btn btn-default">Buscar</button>
</form>
<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th width="20px">ID</th>
        <th>Nombre</th>
        <th>Presentacion</th>
        <th>Categoria</th>
        <th colspna="">Acciones</th>
    </tr>
    <tbody>
    @foreach($medicamentos as $medicamento)
        <tr>
            <td>{{ $medicamento->id }}</td>
            <td style="width:27%">{{ $medicamento->Nombre }}</td>
            <td style="width:40%">{{ $medicamento->prentaciones->nombre }}</td>
            <td style="width:100%">{{ $medicamento->categorias->nombre }}</td>

            {{-- <td>
                <a href="{{ route('categorias.show', $categoria->id)}}" class="btn btn-info pull-right">Ver</a>
            </td> --}}
            <td>
                <a href="{{ route('medicamentos.edit', $medicamento->id)}}" class="btn btn-warning pull-right">Editar</a>
            </td>
            <td>
                <form action="{{ route('medicamentos.destroy', $medicamento->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method"value="DELETE">
                    <button class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
    </thead>
</table>
{!! $medicamentos->render() !!}
@endsection