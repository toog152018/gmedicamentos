@extends('layouts.layout')

@section('title')
Venta-Facturacion
@endsection

@section('content')

<h2>Factura</h2>
<form class="form-inline" style="width:100%" action="{{ route('ventas.store' )}}" method="POST">
	{{ csrf_field() }}
{{--	<div class="form-group">
		<label for="fecha">Fecha :</label><br>
		<input type="date" class="form-control"  name="fecha">
	</div>--}}
	{{--tabla bucador--}}
    @include('ventas.Modal.buscador')



    <br><br>
	<table class="table bg-info"  id="tabla">
		<thead>
		<tr>
			<th width="20px">ID</th>
			<th>Nombre</th>
			<th>Presentacion</th>
			<th>Precio</th>
			<th>Descuento</th>
			<th>Cantidad</th>
			<th>Subtotal</th>
			<th colspna="">Acciones</th>
		</tr>
		<tbody>
		</tbody>
		</thead>
				</table>
<div align="rigth" class="form-inline" >
	<label for="nombre">Total:</label>
	<input type="text" class="form-control" style="width:20%" name="total" id="total" readonly value="0">
	<div class="form-group">
		<label for="f_pago">Forma de pago :</label>
		<select class="form-control selecc" id="f_pago" name="f_pago" required>
			<option value="Tarjeta de credito">Tarjeta de credito</option>
			<option value="Efectivo">Efectivo</option>
		</select>
	</div><br>
</div>
	<br>
	{{--<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Más + </button>--}}
	<button type="submit" class="btn btn-success">Guardar</button>
	<button type="button" class="btn btn-secondary"><a style="text-decoration:none;color:black" href="{{ route('ventas.index')}}">Cancelar</a></button>
</form>

<script>


		$(function(){
			// Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
			$("#adicional").on('click', function(){
				$("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
			});

			// Evento que selecciona la fila y la elimina
			$(document).on("click",".eliminar",function(){
				var parent = $(this).parents().get(0);
				var subtotal=$(this).parents("tr").find("td")[6].innerHTML;
				console.log(subtotal);
                // $(parent).remove();
			});

            $(document).on("click",".agregar",function(){
                var parent = $(this).parents().get(0);
                var id = $(this).parents("tr").find("td")[0].innerHTML;
                var nombre = $(this).parents("tr").find("td")[1].innerHTML;
                var presentacion = $(this).parents("tr").find("td")[2].innerHTML;
                var precio = $(this).parents("tr").find("td")[3].innerHTML;
                var descuento = $(this).parents("tr").find("td")[4].innerHTML;
                var existencia = $(this).parents("tr").find("td")[5].innerHTML;
				var cantidad = $("#cantidad"+id).val();
                var total=parseFloat($("#total").val()) ;
                var descuentoA=0;
                if (document.getElementById("descuento"+id).checked)
                {
                    descuentoA=parseFloat(descuento*precio).toFixed(2);
                    var subtotal=parseFloat(cantidad*precio).toFixed(2)-parseFloat(descuento*precio).toFixed(2);

                }else {
                    var subtotal = parseFloat(cantidad * precio).toFixed(2);
                }

                var too=parseFloat(total )+parseFloat(subtotal);
                $("#total").val(too);


                // var linea="<tr>"+"<td>"+id+"</td>"+ "<td style="+"width:27%">"+nombre+"</td>";
				var linea="<tr>" +
					"<td><input readonly name='id[]' value=' "+id +"'></td>"+
                    "<td><input readonly name='nombre[]' value=' "+nombre +"'></td>"+
                    "<td><input readonly name='presentacion[]' value=' "+presentacion+"'></td>"+
                    "<td><input readonly name='precio[]' value=' "+precio +"'></td>"+
                    "<td><input readonly name='descuento[]' value=' "+descuentoA +"'></td>"+
                    "<td><input readonly name='cantidades[]' value=' "+cantidad +"'></td>"+
                    "<td><input readonly name='subtotal[]' value=' "+ subtotal +"'></td>"+
					"<td class=\"eliminar \"><button type=\"button\" class=\"btn btn-danger\">Menos - </button></td>"+
                    "</tr>";
				var linea2='';

                //    $(parent).remove();
				console.log(total);
				console.log(nombre);
				console.log(presentacion);
				console.log(precio);
				console.log(descuento);
				console.log(existencia);
				console.log(cantidad);
				console.log(linea);
                $(parent).remove();

				$("#tabla tbody ").append(linea);
            });
		});
		</script>

<script type="text/javascript">

    $("#buscar").on('click',function(){

        $value=$("#nombre").val();
        $.ajax({

            type : 'get',

            url : '{{URL::to('buscador')}}',

            data:{'nombre':$value},

            success:function(data){

                $("#tbuscador tbody").html(data);

            }

        });



    })

</script>

<script type="text/javascript">

    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

</script>
@endsection

