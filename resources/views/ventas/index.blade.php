@extends('layouts.layout')

@section('title')
Ventas
@endsection

@section('title-1')
GESTION DE VENTAS
@endsection

@section('content')
	
	<div class="clo-sm-8">
		<h2>
			Lista de ventas
			<a href="{{ route('ventas.create')}}" class="btn btn-primary pull-right">Nuevo</a>
		</h2>		

	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20px">ID</th>
				<th>Fecha </th>
				<th>Vendedor</th>
				<th>Total</th>
				<th>Forma pago</th>
				<th colspna="3">Acciones</th>
			</tr>
			<tbody>
				@foreach($ventas as $venta)
				<tr>
					<td>{{ $venta->id }}</td>
					<td>{{ $venta->fecha }}</td>
					<td>{{ $venta->vendedor_id }}</td>
					<td>{{ $venta->total}} </td>
					<td>{{$venta->forma_pago}}</td>
					<td>
						<a href="{{ route('ventas.show', $venta->id)}}" class="btn btn-warning pull-right">Detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
	{{$ventas->render()}}
	</div>
@endsection