@extends('layouts.layout')
@section('title')
Venta - Detalle
@endsection

@section('content')
<h2>Detalles Venta</h2>
		<label>FACTURA No.{{$detalles->id}}</label>
<label>Vendida por codico empleado: {{$detalles->vendedor_id}}</label>
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Presentacion</th>
				<th>Descuento</th>
				<th>Cantidad</th>
				<th>Subtotal</th>
			</tr>
			<tbody>
				@foreach($detalles->detalleVenta as $de)
				<tr>
					<td>{{$de->idProducto}}</td>
					<td></td>
					<td>{{$de->descuento}}</td>
					<td>{{$de->cantidad}}</td>
					<td>{{$de->precio}}</td>
				{{--	<td>{{ $de['descripcion'] }}</td>
					<td>{{ $de['fechaVencimiento'] }}</td>
					<td>{{ $de['precio'] }}</td>
					<td>{{ $de['cantidad'] }} </td>--}}
				</tr>
				@endforeach
			</tbody>
		</thead>
	</table>
<label>Total de la venta: {{$detalles->total}}</label><br>
<label>Forma de pago:  {{$detalles->forma_pago}}</label><br>
		<button type="button" class="btn btn btn-success"><a style="text-decoration:none;color:black" href="{{ route('ventas.index')}}">Regresar</a></button>


@endsection