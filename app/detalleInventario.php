<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleInventario extends Model
{
    //
    	 protected $table = 'inventario';

         protected $fillable = ['idmedicamento', 'idsucursal' ,'CantidadMinima', 'Nombre',];

         public function inventario()
         {
         	return $this->belongsTo('App\inventario', 'idmedicamento', 'cls
         	');

         }

}