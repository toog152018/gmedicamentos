<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
    //
     	 protected $table = 'lotes';

         protected $fillable = ['fechaVencimiento','descripcion'];

         public function detalleCompra()
         {
         	return $this->hasMany('App\DetalleCompra', 'lotes_id', 'id');
         }

    public function catalogoExistencias()
    {
        return $this->hasMany('App\CatalogoExistencia', 'idLote', 'id');
    }
}
