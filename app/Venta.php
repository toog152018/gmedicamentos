<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    //
 	 protected $table = 'ventas';
     protected $fillable = ['fecha', 'vendedor_id', 'total','forma_pago','id'];

     public function detalleVenta()
     {
     	return $this->hasMany('App\DetalleVenta', 'idVenta', 'id');
     }

     public function detalleUsuario(){
     	return $this->hasMany('App\User', 'id', 'name');
     }
}
