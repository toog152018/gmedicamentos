<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Caffeinated\Shinobi\Traits\ShinobiTrait;


class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;
    
     protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

         public function DetalleVentas()
     {
        return $this->belongsTo('App\Venta',  'id', 'fecha','total');
     }

}
