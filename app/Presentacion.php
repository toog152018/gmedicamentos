<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model
{
    protected $fillable = [
        'nombre', 'descripcion',
    ];

    public function medicamentos()
    {
        return $this->hasMany('App\Mediacmento', 'idPresentacion', 'id');
    }


}
