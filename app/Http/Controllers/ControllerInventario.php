<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\inventario;
use App\detalleInventario;

class ControllerInventario extends Controller
{
    
    public function index()
    {
        //
        $inventario = inventario::orderBy('idmedicamento','DESC')->paginate(10);
        return view('inventario.index', compact('inventario'));
    }

    public function create()
    {
        //
        return view('inventario.create');
    }

    public function store(Request $request)
    {
        //
        $data = request()->all();
        $idmedicamento = $data['idmedicamento'];
        $idsucursal = $data['idsucursal'];
        $cantidad = $data['cantidadMin'];
        $nombre = $data['Nombre'];
        $fechregistro = $data['fecha_registro'];
       
        #Crear la inventario
        $inventario = inventario::create([
            'idmedicamento' => $data['idmedicamento'],
            'idsucursal' => $data['idsucursal'],
            'cantidadMin' => $data['cantidadMin'],
              'Nombre' => $data['Nombre'],
            'fecha_registro' => $data['fecha_registro'],
            ]);
       
        return redirect()->route('inventario.index');
    }

    public function edit($id)
    {
        //
        $inventario = inventario::find($idmedicamento);
        return view('inventario.edit', compact('inventario'));
    }

    public function update(Request $request, $id)
    {
        //
        $data = request()->all();

        $inventario = inventario::find($idmedicamento);
        $inventario->update([
            'cantidadMin'      => $data['cantidadMin'],
            'Nombre'=>$data['Nombre'],
            'fecha_registro'=>$data['fecha_registro'],
            ]);
        return redirect()->route('inventario.index');
    }

    public function destroy($id)
    {
        //
        $inventario = inventario::find($idmedicamento);
        $inventario->delete();
        return back()->with('info', 'El medicamento: '.$inventario->idmedicamento.', fue eliminado');
    }
    public function show($id)
    {

          $inventario = inventario::find($id);
         return view('inventario.show', ['inventario'=>$this->inventario]);
    }
        

        
}