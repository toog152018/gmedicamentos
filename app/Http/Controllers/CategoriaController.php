<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function index(){
    	$categorias = Categoria::orderBy('id', 'DESC')->paginate(5);
    	return view('categorias.index', compact('categorias'));
    }
    public function destroy($id){
    	$categoria = Categoria::find($id);
    	$categoria->delete();
    return back()->with('info', 'La categoria: '.$categoria->id.', fue eliminada');
    }
    public function create(){
        return view('categorias.create');
    }

    public function store(){
        $data = request()->all();
        Categoria::create([
            'nombre'      => $data['nombre'],
            'descripcion' => $data['descripcion'],
            ]);
        return redirect()->route('categorias.index');
    }

    public function edit($id){
        $categoria = Categoria::find($id);
        return view('categorias.edit', compact('categoria'));
    }

    public function update($id){
        $data = request()->all();

        $categoria = Categoria::find($id);
        $categoria->update([
            'nombre'      => $data['nombre'],
            'descripcion' => $data['descripcion'],
            ]);
        return redirect()->route('categorias.index');
    }
}
