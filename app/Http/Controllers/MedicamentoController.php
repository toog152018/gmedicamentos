<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicamento;
use App\Presentacion;
use App\Categoria;

class MedicamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $medicamentos = Medicamento::orderBy('id','DESC')->paginate(10);
        //$medicamentos = DB::table('medicamentos')
         //   ->join('categorias','categorias.id','=','medicamentos.idCategoria')
          //  ->select('*')
          //  ->get();
        return view('medicamentos.index', compact('medicamentos'));    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Presentaciones=Presentacion::orderBy('id','DESC')->paginate(10);
        $Categorias=Categoria::orderBy('id','DESC')->paginate(10);
        return view('medicamentos.create',compact('Presentaciones','Categorias'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = request()->all();
        Medicamento::create([
            'idPresentacion' => $data['presentacion'],
            'idCategoria' =>$data['categoria'],
            'Nombre' =>$data['nombre'],
            'idSucursal' =>1,
        ]);
        return redirect()->route('medicamentos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $medicamento = Medicamento::find($id);
        $Presentaciones=Presentacion::orderBy('id','DESC')->get();
        $Categorias=Categoria::orderBy('id','DESC')->get();
        return view('medicamentos.edit', compact('medicamento','Presentaciones','Categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = request()->all();

        $medicamento = Medicamento::find($id);
        $medicamento->update([
            'idPresentacion' => $data['presentacion'],
            'idCategoria' =>$data['categoria'],
            'Nombre' =>$data['nombre'],
        ]);
        return redirect()->route('medicamentos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $medicamento = Medicamento::find($id);
        $medicamento->delete();
        return back()->with('info', 'El medicamento: '.$medicamento->id.', fue eliminado');
    }
}
