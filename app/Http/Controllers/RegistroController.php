<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;



class RegistroController extends Controller
{

    public function index()
    {
        $registros=Registro::orderBy('id')->paginate(200);
        return view('kardex.index', compact('registros'));

    }


    public function create()
    {

    }


    public function store(Request $request)
    {

    }


    public function show($id)
    {

    }


    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {

    }
}
