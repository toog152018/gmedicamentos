<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;
use App\Medicamento;
use App\Lote;
use App\CatalogoExistencia;
use DB;

class CatalogoExistenciaController extends Controller
{

    public function index(Request $request)
    {
        $nombre=$request->get('nombre');


        $catalogo=CatalogoExistencia::select('medicamentos.Nombre','catalogo_existencia.id','catalogo_existencia.idLote','catalogo_existencia.precioC','catalogo_existencia.precioV','catalogo_existencia.cantidad','catalogo_existencia.descuento')
        ->join('medicamentos','medicamentos.id','=','catalogo_existencia.idMedicamento')
        ->where('medicamentos.Nombre','LIKE','%'.$nombre.'%')
        ->orderBy('id')
        ->get();


                return view('catalogo.index', compact('catalogo'));

        }

    public function create()
    {
        $medicamentos = Medicamento::orderBy('id','DESC')->get();
        $lotes =Lote::orderBy('id','DESC')->get();
        return view('catalogo.create', compact('medicamentos','lotes'));
    }


    public function store(Request $request)
    {
        $data = request()->all();
        CatalogoExistencia::create([
            'idMedicamento'      => $data['medicamento'],
            'idLote' => $data['lote'],
            'precioC' => $data['precioC'],
            'precioV' => $data['precioC']*1.30,
            'cantidad' => $data['cantidad'],
            'descuento' => $data['descuento'],

        ]);

        $catalogo = CatalogoExistencia::find(DB::table('catalogo_existencia')->max('id'));

        Registro::create([
           'idMedicamento' => $catalogo->id,
            'precio' =>$catalogo->precioC,
            'cantidad' => $catalogo->cantidad,
            'total' => $catalogo->precioC*$catalogo->cantidad,
            'tipo' => 'IN',
        ]);

        return redirect()->route('catalogo.index');
    }


}
