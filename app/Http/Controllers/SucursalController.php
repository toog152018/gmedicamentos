<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sucursal;

class SucursalController extends Controller
{
 
    public function index()
    {
        $sucursales = Sucursal::orderBy('id', 'DESC')->paginate(5);
        return view('sucursal.index', compact('sucursales'));
    }


    public function create()
    {
        return view('sucursal.create');
    }

 
    public function store(Request $request)
    {
       $this->validate($request,[ 'lugar'=>'required','nombre'=>'required', 'encargado'=>'required']);
        Sucursal::create($request->all());
        return redirect()->route('sucursal.index')->with('success','Registro creado satisfactoriamente');
      
    }


    public function show($id)
    {
        $sucursales=Sucursal::find($id);
        return  view('sucursal.show',compact('sucursales'));
    }


    public function edit($id)
    {
        $sucursal=Sucursal::find($id);
        return view('sucursal.edit',compact('sucursal'));
    }

 
    public function update(Request $request, $id)
    {
       $this->validate($request,[ 'lugar'=>'required','nombre'=>'required', 'encargado'=>'required']);
 
        Sucursal::find($id)->update($request->all());
        return redirect()->route('sucursal.index')->with('success','Registro actualizado satisfactoriamente');
 
    }


    public function destroy($id)
    {
        Sucursal::find($id)->delete();
        return redirect()->route('sucursal.index')->with('success','Registro eliminado satisfactoriamente');
   
    }
}
