 <?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventario;
class InventarioController extends Controller
{
    //
    public function create(Request $request){
    	$inventario = new Inventario();

    	$inventario -> cantidad = $request -> cantidad;
    	$inventario -> nombre = $request -> nombre;
    	$inventario -> idsucursal = $request -> idsucursal;
    }
}
