<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venta;
use App\User;
use App\DetalleVenta;
use DB; 
class VendedorController extends Controller
{
    
    public function index(Request $request)
    {
       
        $vendedor = User::orderBy('id','DESC')->paginate(10);
        return view('vendedor.index', compact('vendedor'));
       
        /* $vendedor = User::select('ventas.vendedor_id','users.name')
        ->join('ventas','ventas.vendedor.id', '=', 'users.id')
             
        ->get();
        return view('vendedor.index', compact('vendedor'));*/


    }

 
    
    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {

        //$ventas = Venta::select(all);

        $ventas = DB::table('ventas')->where ('vendedor_id', '=', $id)
        ->get();


       return view('vendedor.show', compact('ventas'));

    }


   
}
