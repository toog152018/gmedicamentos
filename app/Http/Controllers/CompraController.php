<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compra;
use App\DetalleCompra;
use App\Lote;

class CompraController extends Controller
{
    
    public function index()
    {
        //
        $compras = Compra::orderBy('id','DESC')->paginate(10);
        return view('compras.index', compact('compras'));
    }

    public function create()
    {
        //
        return view('compras.create');
    }

    public function store(Request $request)
    {
        //
        $data = request()->all();
        $total = 0;        $detalleP = $data['precio'];
        $detalleC = $data['cantidad'];
        #calcular el precio
        while (true) {
            # code...
            $precio = current($detalleP);
            $cantidad = current($detalleC);
            $total = $total + ($precio * $cantidad);
            $precio = next($detalleP);
            $cantidad = next($detalleC);

            if($precio === false && $cantidad === false ) break;
        
        }
        #Crear la compra
        $compra = Compra::create([
            'fecha'      => $data['fecha'],
            'proveedor' => $data['proveedor'],
            'total' => $total,
            ]);
        $lotesD = $data['descripcion'];
        $lotesF = $data['fechaVencimiento'];
        $detalleP = $data['precio'];
        $detalleC = $data['cantidad'];

        while (true) {
            # code...
            $descripcion = current($lotesD);
            $fecha = current($lotesF);
            $precio = current($detalleP);
            $cantidad = current($detalleC);

            $lote = Lote::create([
            'fechaVencimiento'      => $fecha,
            'descripcion' => $descripcion,
            ]);

            DetalleCompra::create([
            'compras_id' => $compra['id'],
            'lotes_id' => $lote['id'],
            'precio' => $precio,
            'cantidad' => $cantidad,
            ]);
            


            $descripcion = next($lotesD);
            $fecha = next($lotesF);
            $precio = next($detalleP);
            $cantidad = next($detalleC);

            if($descripcion === false && $fecha === false && $precio === false && $cantidad === false ) break;

        
        }
/*        error_log("HOLA AQUI EMPIEZA LA IMPRESION DE LO QUE NO SE************************************************************************ ");
        error_log(" ".$total);
        error_log(" ".$compra);
*/        
        return redirect()->route('compras.index');
    }

    public function edit($id)
    {
        //
        $compra = Compra::find($id);
        return view('compras.edit', compact('compra'));
    }

    public function update(Request $request, $id)
    {
        //
        $data = request()->all();

        $compra = Compra::find($id);
        $compra->update([
            'fecha'      => $data['fecha'],
            'proveedor' => $data['proveedor'],
            'total' => $data['total'],
            ]);
        return redirect()->route('compras.index');
    }

    public function destroy($id)
    {
        //
        $compra = Compra::find($id);
        $compra->delete();
        return back()->with('info', 'La compra: '.$compra->id.', fue eliminada');
    }
    public function show($id){

        $detalles = Compra::find($id)->detalleCompra;
        $lotes = array();
        $arreglo = array();
        foreach($detalles as $de) {
            # code...
            $lote = Lote::find($de['lotes_id']);
            $arr = array (
                'descripcion' => $lote['descripcion'],
                'fechaVencimiento' =>$lote['fechaVencimiento'],
                'precio' => $de['precio'],
                'cantidad' => $de['cantidad'],
            );
            array_push($arreglo, $arr);
        }
        
        return view('compras.show', compact('arreglo'));

        

    }
}
