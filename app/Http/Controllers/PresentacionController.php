<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presentacion;

class PresentacionController extends Controller
{
    public function index(){
    	$presentaciones = Presentacion::orderBy('id', 'DESC')->paginate(5);
    	return view('presentaciones.index', compact('presentaciones'));
    }
    public function destroy($id){
    	$presentacion = Presentacion::find($id);
    	$presentacion->delete();
    return back()->with('info', 'La presentacion: '.$presentacion->id.', fue eliminada');
    }
    public function create(){
        return view('presentaciones.create');
    }

    public function store(){
        $data = request()->all();
        Presentacion::create([
            'nombre'      => $data['nombre'],
            'descripcion' => $data['descripcion'],
            ]);
        return redirect()->route('presentaciones.index');
    }

    public function edit($id){
        $presentacion = Presentacion::find($id);
        return view('presentaciones.edit', compact('presentacion'));
    }

    public function update($id){
        $data = request()->all();

        $presentacion = Presentacion::find($id);
        $presentacion->update([
            'nombre'      => $data['nombre'],
            'descripcion' => $data['descripcion'],
            ]);
        return redirect()->route('presentaciones.index');
    }
}
