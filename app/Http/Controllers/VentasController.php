<?php

namespace App\Http\Controllers;

use App\Registro;
use Illuminate\Http\Request;
use App\Venta;
use App\Medicamento;
use App\DetalleVenta;
use App\CatalogoExistencia;


class VentasController extends Controller
{

    public function index()
    {
        $ventas = Venta::orderBy('id','DESC')->paginate(10);
        return view('ventas.index', compact('ventas'));
    }


    public function create(Request $request)
    {
        $nombre=$request->get('nombre');
        $medicamentos=Medicamento::orderBy('id','DESC')
            ->nombre($nombre)
            ->paginate(2);
        return view('ventas.create',compact('medicamentos'));

    }


    public function store(Request $request)
    {
        //
        $data = request()->all();
        $id= $data['id'];
        $precio=$data['precio'];
        $descuento=$data['descuento'];
        $cantidad=$data['cantidades'];
        $subtotal=$data['subtotal'];
        $total=$data['total'];
        $fecha= date('Y-m-d H:i:s');
        $pago=$data['f_pago'];

        $venta=Venta::create([
            'vendedor_id' => 1,
            'fecha'=>$fecha,
            'total' =>$total,
            'forma_pago' =>$pago,
            ]
        );


        while (true) {
            # code...
            $idVenta=$venta->id;
            $idProducto=current($id);
            $precioUni=current($precio);
            $canti=current($cantidad);
            $subt=current($subtotal);
            $des=current($descuento);

            DetalleVenta::create([
                'idVenta'=>$idVenta,
                'idProducto'=>$idProducto,
                'precioUni'=>$precioUni,
                'cantidad'=>$canti,
                'precio'=>$subt,
                'descuento'=>$des,
            ]);
            $existencia=CatalogoExistencia::find($idProducto);
            $n=$existencia->cantidad;
            $existencia->cantidad=$n-$canti;
            $existencia->save();

            Registro::create([
                'idMedicamento'=>$idProducto,
                'precio'=>$precioUni,
                'cantidad'=>$canti,
                'total'=>$subt,
                'tipo'=>'OUT',
            ]);

            $idProducto=next($id);
            $precioUni=next($precio);
            $canti=next($cantidad);
            $subt=next($subtotal);
            $des=next($descuento);


            if($idProducto === false && $precioUni === false && $canti === false && $subt === false && $des === false ) break;


        }
        return redirect()->route('ventas.index');


    }


    public function show($id)
    {

        $detalles = Venta::find($id);
        return view('ventas.show', compact('detalles'));

    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function buscador(Request $request)
    {

        if($request->ajax())

        {

            $output="";
            $nombre=$request->get('nombre');


            /*$medicamentos=Medicamento::orderBy('id','DESC')
            ->nombre($nombre)
            ->paginate(5);*/
            $medicamentos=CatalogoExistencia::select('medicamentos.Nombre','catalogo_existencia.id','catalogo_existencia.idLote','catalogo_existencia.precioC','catalogo_existencia.precioV','catalogo_existencia.cantidad','catalogo_existencia.descuento','presentacions.nombre')
                ->join('medicamentos','medicamentos.id','=','catalogo_existencia.idMedicamento')
                ->join('presentacions','presentacions.id','=','medicamentos.idPresentacion')
                ->where('medicamentos.Nombre','LIKE','%'.$nombre.'%')
                ->where('catalogo_existencia.cantidad','>',0)
                ->orderBy('Nombre','ASC')
                ->orderBy('id','ASC')
                ->get();


            if($medicamentos) {
                $i=array();
                $n='';
                $evaluar=$medicamentos;

                foreach ($evaluar as $item)
                {
                    $name=$item->Nombre;
                    if($n!=$name){
                        array_push($i,$item->id);
                        $n=$item->Nombre;

                    }
                }
                array_push($i,0);

                $j=0;
                foreach ($medicamentos as $key => $medi) {

                    $output .= '<tr>' .

                        '<td>'. $medi->id .'</td>'.

                        '<td style="width:27%">'. $medi->Nombre .'</td>'.

                        '<td style="width:40%">'. $medi->nombre .'</td>'.

                        '<td style="width:100% display:none">'. $medi->precioV .'</td>'.

                        '<td>'.$medi->descuento.'</td>'.

                        '<td>'.$medi->cantidad.'</td>';

                        if($i[$j]==$medi->id) {

                            $output .=  '<td> <input id="cantidad'.$medi->id.'" name="cantidad'.$medi->id.'" placeholder="Cantidad"></td>

                            <td>Descuento<input type="checkbox" name="descuento'.$medi->id.'" id="descuento'.$medi->id.'"></td>

                            <td class="agregar "><button id="adicional" name="adicional" type="button" class="btn btn-warning"> Agregar </button></td>
                            
                            ';

                            $j++;

                     }
                    $output .= '</tr>';

                }
                return Response($output);

            }else{
                $output="<tr> <td></td> <td>No se encontro resultado o no se encuentran en existencia</td><td></td><td></td><td></td></tr>";
                return Response($output);

            }


        }

    }

    public function movimiento(Request $request)
    {
        $nombre=$request->get('nombre');

        $medicamentos=Medicamento::orderBy('id','DESC')
            ->nombre($nombre)
            ->paginate(2);
        return view('ventas.movimientos',compact('medicamentos'));
    }
}
