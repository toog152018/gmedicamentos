<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    //
    	 protected $table = 'detalle_ventas';

         protected $fillable = ['precioUni', 'cantidad' ,'precio','descuento','idVenta', 'idProducto',];

         public function ventas()
         {
         	return $this->belongsTo('App\Venta', 'idVenta', 'id');

         }

	         /*public function lotes()
	         {
	         	return $this->belongsTo('App\Lote', 'idMedicamento', 'id');

	         }*/
}
