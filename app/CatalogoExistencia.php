<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogoExistencia extends Model
{
    //
    protected $table = 'catalogo_existencia';
    protected $fillable = ['idMedicamento', 'precioC', 'precioV','descuento','cantidad','idLote',];

    public function lotes()
    {
        return $this->belongsTo('App\Lote', 'idLote', 'id');

    }

    public function medicamentos()
    {
        return $this->belongsTo('App\Medicamento', 'idMedicamento', 'id');

    }

    public function registro()
    {
        return $this->hasOne('App\Registro', 'idMedicamento', 'id');
    }

    //Scope
    public function scopeMedicamento($query, $medi)
    {
        if ($medi)
            return $query->where('medicamentos','LIKE',"%$medi%");

    }
}
