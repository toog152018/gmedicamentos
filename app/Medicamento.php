<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    //
    protected $table = 'medicamentos';
    protected $fillable = ['idPresentacion', 'idCategoria', 'Nombre','idSucursal'];


    public function prentaciones()
    {
        return $this->belongsTo('App\Presentacion', 'idPresentacion', 'id');

    }

    public function categorias()
    {
        return $this->belongsTo('App\Categoria', 'idCategoria', 'id');

    }

    public function catalogoExistencias()
    {
        return $this->hasMany('App\CatalogoExistencia', 'idMedicamento', 'id');

    }

    public function scopeNombre($query,$nombre)
    {
        if($nombre)
            return $query->where('Nombre','LIKE',"%$nombre%");
    }
}
