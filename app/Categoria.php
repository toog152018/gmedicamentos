<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        'nombre', 'descripcion',
    ];

    public function medicamentos()
    {
        return $this->hasMany('App\Mediacmento', 'idCategoria', 'id');
    }
}
