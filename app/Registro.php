<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    //
    protected $table = 'registro';
    protected $fillable = ['precio', 'cantidad', 'total','tipo','idMedicamento'];


    public function catalogoExistencias()
    {
        return $this->belongsTo('App\CatalogoExistencia', 'idMedicamento', 'id');

    }



}
