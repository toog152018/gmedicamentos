<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    //
    	 protected $table = 'detalleCompras';

         protected $fillable = ['precio', 'cantidad' ,'compras_id', 'lotes_id',];

         public function compras()
         {
         	return $this->belongsTo('App\Compra', 'compras_id', 'id');

         }

         public function lotes()
         {
         	return $this->belongsTo('App\Lote', 'lotes_id', 'id');

         }
}
