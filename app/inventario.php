<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventario extends Model
{
  protected $table = 'inventario';
    protected $fillable = [
       'idmedicamento', 'idsucursal','Nombre', 'cantidadMinima','fecha_registro'
    ];

    public function detalleInventario()
         {
         	return $this->hasMany('App\inventario', 'idmedicamento', 'idmedicamento');
         }
   
}
